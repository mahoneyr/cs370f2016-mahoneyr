//Race Mahoney
//Professor Jumadinova
//CMPSC 370
//Lab 1
import java.util.Random;

class SimpleReflexAgent{

    static String[] location = {"Railway", "Subway Stop"};
    static String[] status = {"Moving", "Stopped"};
    static int locationIndex = new Random().nextInt(location.length);
    static int statusIndex = new Random().nextInt(location.length);
    static String randomL = (location[locationIndex]);
    static String randomS = (status[statusIndex]);


    public static void reflexVacuumAgent(String randomL, String randomS){
        System.out.println("Location is: " + randomL);
        System.out.println("Train is currently " + randomS);

        if((randomS == "Moving") && (randomL == "Railway") ) {
            System.out.println("Continue until Subway Stop is reached");
        }
        if((randomS == "Moving") && (randomL == "Subway Stop") ) {
            System.out.println("Stop train immediately to allow passengers on");
        }
        if((randomS == "Stopped") && (randomL == "Railway") ) {
            System.out.println("Speed up until Subway Stop is reached");
        }
        if((randomS == "Stopped") && (randomL == "Subway Stop") ) {
            System.out.println("Allow all passengers to board and then increase speed");
        }

    }//reflex

    public static void main(String args[]){
        reflexVacuumAgent(randomL, randomS);
    }//main

}//class
