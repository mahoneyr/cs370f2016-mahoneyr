import java.io.*;

class POS_NEG {

    public static void main(String[] args){
          String line = null;
          String fileReadName = args[0];

      try {
          FileReader fileReader = new FileReader(fileReadName);
          BufferedReader bufferedReader = new BufferedReader(fileReader);
          //sentiment keywords
          String POS = "Positive";
          String NEG = "Negative";
          String NEU = "Neutral";
          int pos = 0;
          int neg = 0;
          int neu = 0;
          //read each line of the file looking for the sentiment keywords
          //each time a key word is found update the counter
          while((line = bufferedReader.readLine()) != null) {
                pos += line.split(POS, -1).length-1;
                neg += line.split(NEG, -1).length-1;
                neu += line.split(NEU, -1).length-1;

          }//while
          //display how many key words were found
          System.out.println("Number of Positive reviews " + pos);
          System.out.println("Number of Negative reviews " + neg);
          System.out.println("Number of Neutral reviews " + neu);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }//main
}//class
