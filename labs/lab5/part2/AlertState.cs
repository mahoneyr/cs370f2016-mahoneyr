﻿using UnityEngine;
using System.Collections;

public class AlertState : IEnemyState 

{
	private readonly StatePatternEnemy enemy;
	private float searchTimer;
	private int rand = Random.Range(1, 11);
	private int reward;
	private int totalReward;

	public AlertState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		Look ();
		Search ();

	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{
		enemy.currentState = enemy.patrolState;
		searchTimer = 0f;
	}

	public void ToAlertState()
	{
		
	}

	public void ToChaseState()
	{
		enemy.currentState = enemy.chaseState;
		searchTimer = 0f;
	}

	private void Look()
	{	int randLook = Random.Range(1, 11);
		RaycastHit hit;
		if (Physics.Raycast (enemy.eyes.transform.position, enemy.eyes.transform.forward, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player") && randLook >5 ){
			enemy.chaseTarget = hit.transform;
			reward = 8;
			enemy.totalReward =+ reward;
			ToChaseState();
		}
	}

	void Search ()
	{	int randSearch = Random.Range(1, 11);
		enemy.meshRendererFlag.material.color = Color.yellow;
		enemy.navMeshAgent.Stop ();
		enemy.transform.Rotate (0, enemy.searchingTurnSpeed * Time.deltaTime, 0);
		searchTimer += Time.deltaTime;

		if (searchTimer >= enemy.searchingDuration) {
			if (randSearch > 4) {
				reward = 2;
				enemy.totalReward = +reward;
				ToPatrolState ();
			} else
				Search ();
		}
	}
}