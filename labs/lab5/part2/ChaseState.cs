﻿using UnityEngine;
using System.Collections;

public class ChaseState : IEnemyState 
{
	private readonly StatePatternEnemy enemy;
	private int rand = Random.Range(1, 11);
	private int reward;
	private int totalReward;


	public ChaseState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		Look ();
		Chase ();
	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{
		Debug.Log ("Can't transition to same state");
	}

	public void ToAlertState()
	{
		enemy.currentState = enemy.alertState;
	}

	public void ToChaseState()
	{
		Debug.Log ("Can't transition to same state");
	}

	private void Look()
	{	int randLook = Random.Range(1, 11);
		RaycastHit hit;
		Vector3 enemyToTarget = (enemy.chaseTarget.position + enemy.offset) - enemy.eyes.transform.position;
		if (Physics.Raycast (enemy.eyes.transform.position, enemyToTarget, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player") && randLook > 1) {
			reward = 10;
			enemy.totalReward = +reward;
			enemy.chaseTarget = hit.transform;
		} else  {
			reward = 1;
			enemy.totalReward = +reward;
			ToAlertState();
		}
	}

	void Chase ()
	{
		enemy.meshRendererFlag.material.color = Color.red;
		enemy.navMeshAgent.destination = enemy.chaseTarget.position;
		enemy.navMeshAgent.Resume ();

	}
}
