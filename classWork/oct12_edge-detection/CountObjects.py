import numpy as np
import argparse
import cv2

# Construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required = True,
	help = "Path to the image")
args = vars(ap.parse_args())


image = cv2.imread(args["image"])
image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
image = cv2.GaussianBlur(image, (5, 5), 0)
cv2.imshow("Blurred", image)


canny = cv2.Canny(image, 30, 150)
cv2.imshow("Canny", canny)

contour = cv2.findContours(image.copy(), cv2.RETR_EXTERNAL,
          cv2.CHAIN_APPROX_SIMPLE)
contour = cv2.drawContours(image.copy(), contours, -1, (0,255,0), 3)
cv2.imshow("Contours", contour)
